//
//  ItemMovimientosTableViewCell.swift
//  ModuloPagoIOS
//
//  Created by Charls Salazar on 17/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ItemMovimientosTableViewCell: UITableViewCell {

    @IBOutlet weak var mIconMovementImageView: UIImageView!
    @IBOutlet weak var mDateMovementLabel: UILabel!
    @IBOutlet weak var mAmmountMovementLabel: UILabel!
    @IBOutlet weak var mTitleMovementLabel: UILabel!
    @IBOutlet weak var mAddressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
