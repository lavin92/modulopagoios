 import UIKit
 
 
 class BaseViewController: UIViewController, UITextFieldDelegate {
    
    var mPresenter : BasePresenter?
    var mPresenters : [BasePresenter]! = []
    var extras : [String: AnyObject] = [:]
    var requestValue : String = ""
    var data : [String : AnyObject] = [:]
    var mPresedence : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = ""
        mPresenter = getPresenter()
        mPresenters = getPresenters()
        if mPresenter != nil {
            mPresenter?.viewDidLoad()
        }
        for presenter in mPresenters! {
            presenter.viewDidLoad()
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if mPresenter != nil {
            mPresenter?.viewDidAppear()
        }
        for presenter in mPresenters! {
            presenter.viewDidAppear()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        if mPresenter != nil {
            mPresenter?.viewDidDisappear()
        }
        for presenter in mPresenters! {
            presenter.viewDidDisappear()
        }
    }
    
    func getPresenter() -> BasePresenter? {
        return nil
    }
    
    func getPresenters() -> [BasePresenter]? {
        return []
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if mPresenter != nil {
            mPresenter?.viewWillAppear()
        }
        for presenter in mPresenters! {
            presenter.viewWillAppear()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if mPresenter != nil {
            mPresenter?.viewWillDisappear()
        }
        for presenter in mPresenters! {
            presenter.viewWillDisappear()
        }
    }
    
    func onViewControllerResult() {
        
    }
    
    func onViewControllerResult(params: [String : String]?) {
        
    }
    
    func hasExtra(key: KeysEnum) -> Bool{
        return self.extras[key] != nil
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    func resignFirstResponser(textFields : UITextField...){
        for textField in textFields{
            textField.resignFirstResponder()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func ChangeSizeLabel(label : UILabel, size : CGFloat){
        if((self.view.frame.width) > CGFloat(430)){
            label.font = label.font.withSize(size)
        }
    }
    
    func ChangeSizeButton(button : UIButton, size : CGFloat){
        if((self.view.frame.width) > CGFloat(430)){
            button.titleLabel?.font = button.titleLabel?.font.withSize(size)
        }
    }
}
public enum KeysEnum: APIKeys {
    
    case EXTRA_WORK_ORDER
    case EXTRA_TIME_TO_ARRIVE_RESPONSE
    case EXTRA_ARR_FAMILY
    case EXTRA_DIRECCION_BEAN
    case EXTRA_PLAN
    case EXTRA_CAPTURE_IMAGE
    case EXTRA_TYPE_PLAN_SELECTED
    case EXTRA_MAP
    case EXTRA_CARD
    case EXTRA_MONTH
}

public extension Dictionary {
    
    public subscript(key: APIKeys) -> Value? {
        get {
            return self[String(describing: key) as! Key]
        }
        set(value) {
            guard
                let value = value else {
                    self.removeValue(forKey: String(describing: key) as! Key)
                    return
            }
            
            self.updateValue(value, forKey: String(describing: key) as! Key)
        }
    }
}


public protocol APIKeys {}
