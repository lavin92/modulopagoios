//
//  BasePresenter.swift
//  ModuloPagoIOS
//
//  Created by Charls Salazar on 20/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class BasePresenter: NSObject {
    
    public override init(){
       
    }
    
    open func viewDidLoad(){
    }
    
    open func viewWillAppear(){
    }
    
    open func viewDidAppear(){
    }
    
    open func viewWillDisappear(){
    }
    
    open func viewDidDisappear(){
    }
    
    open func viewDidUnload(){
    }
    
}
