//
//  BaseResponse.swift
//  VentasTPEmpresarial
//
//  Created by Charls Salazar on 7/6/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

open  class BaseResponse: NSObject, Mappable {
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    open  func mapping(map: Map) {
    
    }
    
}
