//
//  RetrofitManager.swift
//  VentasTPEmpresarial
//
//  Created by Charls Salazar on 7/6/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

class RetrofitManager<Res : BaseResponse>:  BaseRetrofitManager<Res> {
    
    override open func getDebugEnabled() -> Bool {
        return false
    }

}
