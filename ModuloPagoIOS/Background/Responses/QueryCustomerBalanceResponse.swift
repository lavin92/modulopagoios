//
//  QueryCustomerBalanceResponse.swift
//  ModuloPagoIOS
//
//  Created by Charls Salazar on 21/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class QueryCustomerBalanceResponse: BaseResponse {
    var mResultCode : String?
    var mResulDesc : String?
    var mHeader : HeaderBalance?
    var mAccounBalanceList : [AccountBalance] = []
    var mIdentityId : String?
    
    override func mapping(map: Map) {
        mResultCode <- map["resultCode"]
        mResulDesc <- map["resultDesc"]
        mHeader <- map["header"]
        mAccounBalanceList <- map["accountBalanceList"]
        mIdentityId <- map["identityId"]
    }
    
}

class HeaderBalance: NSObject, Mappable {
   
    var mCommandID : String?
    var mOrderID : String?
    var mResponseCode : String?
    var mResponseDesc : String?
    var mSuccess : String?
    
    required init?(map: Map) {
           
    }
    
    func mapping(map: Map) {
        mCommandID <- map["commandID"]
        mOrderID  <- map["orderId"]
        mResponseCode <- map["responseCode"]
        mResponseDesc <- map["responseDesc"]
        mSuccess <- map["success"]
    }
}

class AccountBalance: NSObject, Mappable{
    
    var mAccountTypeName : String?
    var mAccountTypeAlias : String?
    var AccountNo : String?
    var mAccountName : String?
    var mAccountStatus : String?
    var mCurrency : String?
    var mAvailableBalance : String?
    var mReservedBalance : String?
    var mUnclearedBalance : String?
    var mCurrentBalance : String?
    var mAccountHolderID : String?
    var mDefaultFlag : String?
    var mAccountholderPublicName : String?
    
    required init?(map: Map) {
        
    }
    
    
    func mapping(map: Map) {
        mAccountTypeName <- map["accountTypeName"]
        mAccountTypeAlias <- map["accountTypeAlias"]
        AccountNo <- map["accountNo"]
        mAccountName <- map["accountName"]
        mAccountStatus <- map["accountStatus"]
        mCurrency <- map["currency"]
        mAvailableBalance <- map["availableBalance"]
        mReservedBalance <- map["reservedBalance"]
        mUnclearedBalance <- map["unclearedBalance"]
        mCurrentBalance <- map["currentBalance"]
        mAccountHolderID <- map["accountHolderID"]
        mDefaultFlag <- map["defaultFlag"]
        mAccountholderPublicName <- map["accountHolderPublicName"]
    }
}
