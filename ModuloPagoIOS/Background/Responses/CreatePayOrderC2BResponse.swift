//
//  CreatePayOrderC2BResponse.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 20/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//


import Foundation
import ObjectMapper

class CreatePayOrderC2BResponse: BaseResponse {
    var mSignType: String?
    var mNonceStr: String?
    var mResult: String?
    var mCode: String?
    var mSign: String?
    
    required init?(map: Map) {
       }
    
    
    override func mapping(map: Map){
        mSignType <- map["sign_type"]
        mNonceStr <- map["nonce_str"]
        mResult <- map["result"]
        mCode <- map["code"]
        mSign <- map["sign"]
        
        
    }
    
}
