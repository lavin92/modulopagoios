//
//  HistoryMovementsResponse.swift
//  ModuloPagoIOS
//
//  Created by Charls Salazar on 17/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class HistoryMovementsResponse: BaseResponse {
    var mSignType : String?
    var mCode : String?
    var mSign : String?
    var noneStr: String?
    var mMsg : String?
    var mResult : String?
    var mBizContentHistory : BizContentHistory?
    
    override func mapping(map: Map) {
        mSignType <- map["sign_type"]
        mCode <- map["code"]
        mSign <- map["sign"]
        noneStr <- map["nonce_str"]
        mMsg <- map["msg"]
        mResult <- map["result"]
        mBizContentHistory <- map["biz_content"]
    }
}

class BizContentHistory : NSObject, Mappable{
    
    required init?(map: Map) {
    }
    
    var mHistory : [HistoryMovements] = []
    var mSize : String?
    
    func mapping(map: Map) {
        mHistory <- map["history"]
        mSize <- map["size"]
    }
}

class HistoryMovements : NSObject, Mappable{
    
    var mId : String?
    var mMerchOrderId : String?
    var mPaymentOrderId : String?
    var mOrderAmount : String?
    var mDiscountAmount : String?
    var mTotalAmount : String?
    var mTransDirection : String?
    var mTransCurrency : String?
    var mOrderStatus : String?
    var mPaySuccessTime : String?
    var mPayeeName : String?
    var mTradeType : String?
    var mOrderTitle : String?
    
    required init?(map: Map) {
          
    }
      
    func mapping(map: Map) {
        mId <- map["id"]
        mMerchOrderId <- map["merch_order_id"]
        mPaymentOrderId <- map["payment_order_id"]
        mOrderAmount <- map["order_amount"]
        mDiscountAmount <- map["discount_amount"]
        mTotalAmount <- map["total_amount"]
        mTransDirection <- map["trans_direction"]
        mTransCurrency <- map["trans_currency"]
        mOrderStatus <- map["order_status"]
        mPaySuccessTime <- map["pay_success_time"]
        mPayeeName <- map["payee_name"]
        mTradeType <- map["trade_type"]
        mOrderTitle <- map["order_title"]
    }
}
