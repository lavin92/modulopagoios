//
//  BaseRetrofitManager.swift
//  VentasTPEmpresarial
//
//  Created by Charls Salazar on 7/6/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

open  class BaseRetrofitManager<Res : BaseResponse>: NSObject {
    
    open var requestUrl : String
    open var delegate : AlamofireResponseDelegate
    open var request : Alamofire.Request?
    
    public init(requestUrl: String, delegate : AlamofireResponseDelegate){
        self.delegate = delegate
        self.requestUrl = requestUrl
    }
    
    open func getJsonDebug(requestUrl : String) -> String {
        return ""
    }
    
    open func getDebugEnabled() -> Bool {
        return false
    }
    
    open var Manager: Alamofire.SessionManager = {
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "https://189.203.181.233": .disableEvaluation,
            "189.203.181.233": .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        
        return manager
    }()
    
    open func request(requestModel : BaseRequest) {
        
        if getDebugEnabled() {
            let json : String = getJsonDebug(requestUrl : self.requestUrl)
            if json != ""{
                let response = Res(JSONString: json)
                self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response!)
            }
            return
        }
        
        if BaseConnectionUtils.isConnectedToNetwork(){
            delegate.onRequestWs()
            
            let params  = Mapper().toJSON(requestModel)
            print("REQUEST = \(String(describing: params))")
            print("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")
            
            request = Manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinitions.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response.result.value! as Res)
                case .failure(_):
                    self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "Se excedio el tiempo de espera: No fue posible establecer conexión con el servidor?")
                }
            }
        }else {
            delegate.onErrorConnection()
        }
    }
    
    open func requestGet() {
        
        if getDebugEnabled() {
            let json : String = getJsonDebug(requestUrl : self.requestUrl)
            if json != ""{
                let response = Res(JSONString: json)
                self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response!)
            }
            return
        }
        
        if BaseConnectionUtils.isConnectedToNetwork(){
            delegate.onRequestWs()
            
            request = Manager.request(self.requestUrl, method: HTTPMethod.get, encoding: JSONEncoding.default, headers: ApiDefinitions.CUSTOM_HEADERS).responseObject {
                (response: DataResponse<Res>) in
                switch response.result {
                case .success:
                    self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response.result.value! as Res)
                case .failure(_):
                    self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "Se excedio el tiempo de espera: No fue posible establecer conexión con el servidor?")
                }
            }
        }else {
            delegate.onErrorConnection()
        }
    }
    
    
    
    
    /*open func requestRealm(requestModel : BaseRealmRequest) {
     if getDebugEnabled() {
     let json : String = getJsonDebug(requestUrl : self.requestUrl)
     if json != ""{
     let response = Res(JSONString: json)
     self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response!)
     }
     return
     }
     if BaseConnectionUtils.isConnectedToNetwork(){
     delegate.onRequestWs()
     
     let params  = Mapper().toJSON(requestModel)
     print("REQUEST = \(String(describing: params))")
     print("///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////")
     request = Manager.request(self.requestUrl, method: HTTPMethod.post, parameters: params, encoding: JSONEncoding.default, headers: ApiDefinition.CUSTOM_HEADERS).responseObject {
     (response: DataResponse<Res>) in
     switch response.result {
     case .success:
     self.delegate.onSuccessLoadResponse(requestUrl : self.requestUrl, response: response.result.value! as Res)
     case .failure(_):
     self.delegate.onErrorLoadResponse(requestUrl : self.requestUrl, messageError : "")
     }
     
     }
     } else {
     delegate.onErrorConnection()
     }
     }*/
    
}


public protocol AlamofireResponseDelegate {
    
    func onRequestWs()
    
    func onSuccessLoadResponse(requestUrl : String, response : BaseResponse)
    
    func onErrorLoadResponse(requestUrl : String, messageError : String)
    
    func onErrorToken(requestUrl: String, messageError : String)
    
    func onErrorConnection()
    
    
    
}
