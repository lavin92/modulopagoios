//
//  CreatePayOrderP2PRequest.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 20/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper

class CreatePayOrderP2PRequest: BaseRequest {
    var mTimeStamp: String?
    var mMethod: String?
    var mNoceStr: String?
    var mSignType: String?
    var mSign: String?
    var mVersion: String?
    var mBizContent: BizContent?
    
    init(mTimeStamp: String, mMethod: String, mNoceStr: String, mSignType: String, mSign: String, mVersion: String, mBizContent: BizContent) {
        
        super.init()
        self.mTimeStamp = mTimeStamp
        self.mMethod = mMethod
        self.mNoceStr = mNoceStr
        self.mSignType = mSignType
        self.mSign = mSign
        self.mVersion = mVersion
        self.mBizContent = mBizContent
    }
    
    public required init?(map: Map){
        fatalError("init(map:) has not been implemented")
    }
    
    override func mapping(map: Map) {
        mTimeStamp <- map["timestamp"]
        mMethod <- map["method"]
        mNoceStr <- map["nonce_str"]
        mSignType <- map["sign_type"]
        mSign <- map["sign"]
        mVersion <- map["version"]
        mBizContent <- map["biz_content"]
        
    }
    
    
}




//-------------BizContent----------

class BizContent : NSObject, Mappable {
    
    var mAppid: String?
    var mMerchCode: String?
    var mMerchOrderId: String?
    var mPayerIdentifierType: String?
    var mPayerIdentifier: String?
    var mPayerType: String?
    var mPayeeIdentifierType: String?
    var mPayeeIdentifier: String?
    var mPayeeType: String?
    var mInitiatorIdentifierType: String?
    var mInitiatorIdentifier: String?
    var mInitiatorType: String?
    var mInitiatorMerchCode: String?
    var mSecurityCredential: String?
    var mTradeType: String?
    var mTitle: String?
    var mTotalAmount: String?
    var mTransCurrency: String?
    var mTimeoutExpress: String?
    var mBusinessType: String?
    var mCallbackInfo: String?
    
    
    init(mAppid: String, mMerchCode: String, mMerchOrderId: String, mPayerIdentifierType: String, mPayerIdentifier: String, mPayerType: String, mPayeeIdentifierType: String, mPayeeIdentifier: String, mPayeeType: String, mInitiatorIdentifierType: String, mInitiatorIdentifier: String, mInitiatorType: String,mSecurityCredential: String, mTradeType: String, mTitle: String, mTotalAmount: String, mTransCurrency: String, mTimeoutExpress: String, mBusinessType: String, mCallbackInfo: String  ) {
         
        super.init()
        
        self.mAppid = mAppid
        self.mMerchCode = mMerchCode
        self.mMerchOrderId = mMerchOrderId
        self.mPayerIdentifierType = mPayerIdentifierType
        self.mPayerIdentifier = mPayerIdentifier
        self.mPayerType = mPayerType
        self.mPayeeIdentifierType = mPayeeIdentifierType
        self.mPayeeIdentifier = mPayeeIdentifier
        self.mPayeeType = mPayeeType
        self.mInitiatorIdentifierType = mInitiatorIdentifierType
        self.mInitiatorIdentifier = mInitiatorIdentifier
        self.mInitiatorType = mInitiatorType
        //self.mInitiatorMerchCode = mInitiatorMerchCode
        self.mSecurityCredential = mSecurityCredential
        self.mTradeType = mTradeType
        self.mTitle = mTitle
        self.mTotalAmount = mTotalAmount
        self.mTransCurrency = mTransCurrency
        self.mTimeoutExpress = mTimeoutExpress
        self.mBusinessType = mBusinessType
        self.mCallbackInfo = mBusinessType
        
    }
    
    public required init?(map: Map){
        fatalError("init(map:) has not been implemented")
    }
    
    func mapping(map: Map) {
        mAppid <- map["appid"]
        mMerchCode <- map["merch_code"]
        mMerchOrderId <- map["merch_order_id"]
        mPayerIdentifierType <- map["payer_identifier_type"]
        mPayerIdentifier <- map["payer_identifier"]
        mPayerType <- map["payer_type"]
        mPayeeIdentifierType <- map["payee_identifier_type"]
        mPayeeIdentifier <- map["payee_identifier"]
        mPayeeType <- map["payee_type"]
        mInitiatorIdentifierType <- map["initiator_identifier_type"]
        mInitiatorIdentifier <- map["initiator_identifier"]
        mInitiatorType <- map["initiator_type"]
        mInitiatorMerchCode <- map["initiator_merch_code"]
        mSecurityCredential <- map["security_credential"]
        mTradeType <- map["trade_type"]
        mTitle <- map["title"]
        mTotalAmount <- map["total_amount"]
        mTransCurrency <- map["trans_currency"]
        mTimeoutExpress <- map["timeout_express"]
        mBusinessType <- map["business_type"]
        mCallbackInfo <- map["callback_info"]
        
        
        
        
    }
    
    
}




