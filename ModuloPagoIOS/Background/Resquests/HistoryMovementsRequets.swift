//
//  HistoryMovementsRequets.swift
//  ModuloPagoIOS
//
//  Created by Charls Salazar on 17/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

class HistoryMovementsRequets: BaseRequest {
    var mTimeStamp : String?
    var mNonceStr : String?
    var mMethod : String?
    var mSignType : String?
    var mSign : String?
    var mVersion : String?
    var mAppCode : String?
    var mBizContentHistory : BizzContentHistory?
    
    init(mTimeStamp: String, mNonceStr : String, mMethod : String, mSignType : String, mSign: String, mVersion : String, mAppCode : String, mBizContentHistory : BizzContentHistory ){
        super.init()
        self.mTimeStamp = mTimeStamp
        self.mNonceStr = mNonceStr
        self.mMethod = mMethod
        self.mSignType = mSignType
        self.mSign = mSign
        self.mVersion = mVersion
        self.mAppCode = mAppCode
        self.mBizContentHistory = mBizContentHistory
    }
    
    public required init?(map: Map) {
        fatalError("init(map:) has not been implemented")
    }
    
    override func mapping(map: Map) {
        mTimeStamp <- map["timestamp"]
        mNonceStr <- map["nonce_str"]
        mMethod <- map["method"]
        mSignType <- map["sign_type"]
        mSign <- map["sign"]
        mVersion <- map["version"]
        mAppCode <- map["app_code"]
        mBizContentHistory <- map["biz_content"]
    }
    
}

class BizzContentHistory : NSObject, Mappable{

    var mIdentifier : String?
    var mIdentifierType : String?
    var mMerch_code : String?
    var mType : String?
    var mOffset : String?
    var mLimit : String?
    var mStartTime : String?
    var mEndTime : String?
    
    init(mIdentifier : String, mIdentifierType : String, mMerch_code : String,mType : String,  mOffset : String, mLimit : String, mStartTime : String, mEndTime : String ) {
        super.init()
        self.mIdentifier = mIdentifier
        self.mIdentifierType = mIdentifierType
        self.mMerch_code = mMerch_code
        self.mType = mType
        self.mOffset = mOffset
        self.mLimit = mLimit
        self.mStartTime = mStartTime
        self.mEndTime = mEndTime
        
    }
    
    required init?(map: Map) {
           
    }
       
    func mapping(map: Map) {
        mIdentifier <- map["identifier"]
        mIdentifierType <- map["identifier_type"]
        mMerch_code <- map["merch_code"]
        mType <- map["type"]
        mOffset <- map["offset"]
        mLimit <- map["limit"]
        mStartTime <- map["start_time"]
        mEndTime <- map["end_time"]
    }
}
