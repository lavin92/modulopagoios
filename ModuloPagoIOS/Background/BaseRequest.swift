//
//  BaseRequest.swift
//  VentasTPEmpresarial
//
//  Created by Charls Salazar on 7/6/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import ObjectMapper

open  class BaseRequest: NSObject, Mappable {
    
    public func mapping(map: Map) {
    }
    
    override public init() {
    
    }
    
    public required init?(map: Map) {
        
    }
    
}
