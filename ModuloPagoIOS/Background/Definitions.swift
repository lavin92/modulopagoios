


//
//  Definitions.swift
//  ModuloPagoIOS
//
//  Created by Charls Salazar on 15/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class Definitions: NSObject {

    static let WS_CREATE_PAY = "https://alonsoserrano-eval-test.apigee.net/huawei/payment/v1/merchant/createPayOrder"
    static let WS_BALANCE = "https://alonsoserrano-eval-test.apigee.net/huawei/payment/v1/consumers/{id}/accounts"
    static let WS_HISTORY = "https://alonsoserrano-eval-test.apigee.net//huawei/payment/v1/app/queryHistory"

}

class Constantes: NSObject{
    static var mIDPayer = "8615000000218"
    static var mToken = ""
}
