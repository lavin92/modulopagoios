//
//  ApiDefinitions.swift
//  VentasTPEmpresarial
//
//  Created by Charls Salazar on 7/6/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

class ApiDefinitions: NSObject {
    public static let CUSTOM_HEADERS = [
        "Content-Type": "application/json",
        "Accept" : "application/json"
    ]
}
