//
//  AlertDialog.swift
//  VentasTPEmpresarial
//
//  Created by Charls Salazar on 7/3/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//
import UIKit
import PKHUD

public class AlertDialog {
    
    public static var overlay : UIView?
    public static var viewController : UIViewController?
    
    public static func show(title: String, body: String, view : UIViewController, handler: ((UIAlertAction) -> Swift.Void)? = nil){
        let refreshAlert = UIAlertController(title: title, message: body, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        view.present(refreshAlert, animated: true, completion: nil)
    }
    
    @objc public static func pressed(){
        
    }
    
    public static func showOverlay(){
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    public static func hideOverlay(){
        PKHUD.sharedHUD.hide(true)
    }
    
}


