//
//  CollectVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 03/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class CollectVC: UIViewController {
    @IBOutlet weak var mQrImageView: UIButton!
    @IBOutlet weak var mTitleOwnerLabel: UILabel!
    
    @IBOutlet weak var mQRCodeImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        mQRCodeImageView.image = generateQRcode(content: "www.totalpay.com/launch?contentData=" + Constantes.mIDPayer + "<pay<" + Constantes.mToken, widthImage: 500, heightImage: 500)
        
        mTitleOwnerLabel.text = selectUser()
    }
    
    func selectUser()->String{
           switch Constantes.mIDPayer {
           case "8615000000218":
               return "Aurelio"
               
           case "8615000000053":
               return "Eduardo"
               
           case "100002303":
               return "Doña Blanca"
               
           case "100002301":
               return "Crepe Time"
           default:
               return ""
           }
       }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
              return .lightContent
          }
    
    
    @IBAction func clickBackButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Collect", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "CuentaTotalPlayVC") as! CuentaTotalPlayVC
          present(myVC, animated: true, completion: nil)
    }
   
    @IBAction func clickExitButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Collect", bundle: nil)
               let myVC = storyboard.instantiateViewController(withIdentifier: "CuentaTotalPlayVC") as! CuentaTotalPlayVC
                 present(myVC, animated: true, completion: nil)
    }
   
    @IBAction func clickPagarConCelular(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Collect", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PayCellPhoneVC") as! PayCellPhoneVC
          present(myVC, animated: true, completion: nil)
    }
    
    
    @IBAction func sharePressed(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: ["www.Totalplay.com"], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func generateQRcode( content: String, widthImage: CGFloat, heightImage: CGFloat) -> UIImage? {
        let data = content.data(using: String.Encoding.isoLatin1)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            guard let qrCodeImage = filter.outputImage else { return nil }
            let scaleX = widthImage / qrCodeImage.extent.size.width
            let scaleY = heightImage / qrCodeImage.extent.size.height
            let transform = CGAffineTransform (scaleX: scaleX, y: scaleY)
            if let output = filter.outputImage?.transformed(by: transform){
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
}
