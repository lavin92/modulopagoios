//
//  PayCellPhoneVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 08/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class PayCellPhoneVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .lightContent
       }
    
    
    @IBAction func clickButtonExit(_ sender: Any) {
           let storyboard = UIStoryboard(name: "PayCellPhone", bundle: nil)
           let myVC = storyboard.instantiateViewController(withIdentifier: "CuentaTotalPlayVC") as! CuentaTotalPlayVC
           
             present(myVC, animated: true, completion: nil)
       }
    

}
