//
//  ScanerQRVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 13/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ScanerQRVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickCompartirQR(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: ["www.Totalplay.com"], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        
        self.present(activityVC, animated: true, completion: nil)
    }
    

}
