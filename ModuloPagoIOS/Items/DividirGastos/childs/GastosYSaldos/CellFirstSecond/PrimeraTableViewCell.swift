//
//  PrimeraTableViewCell.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 13/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class PrimeraTableViewCell: UITableViewCell {

    @IBOutlet weak var labelFecha: UILabel!
    @IBOutlet weak var imgProducto: UIImageView!
    @IBOutlet weak var labelNomArt: UILabel!
    @IBOutlet weak var labelCliente: UILabel!
    @IBOutlet weak var labelPrecio: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
