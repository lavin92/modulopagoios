//
//  SegundaTableViewCell.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 13/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class SegundaTableViewCell: UITableViewCell {

    @IBOutlet weak var nameUSer1: UILabel!
    @IBOutlet weak var imgFlecha: UIImageView!
    @IBOutlet weak var labelDebe: UILabel!
    @IBOutlet weak var labelCantidad: UILabel!
    @IBOutlet weak var userName2: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
