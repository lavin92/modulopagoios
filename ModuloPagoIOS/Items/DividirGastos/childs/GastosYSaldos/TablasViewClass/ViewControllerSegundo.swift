//
//  ViewControllerSegundo.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 13/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ViewControllerSegundo: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView2: UITableView!
    
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*containerCell.layer.cornerRadius = 10
        containerCell.layer.shadowOffset = CGSize(width: 5, height: 5)
        containerCell.layer.shadowOpacity = 0.5
        containerCell.layer.shadowRadius = 4
        containerCell.clipsToBounds = false
        containerCell.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        */
    }
    
    // MARK: - Table view data source

          func numberOfSections(in tableView: UITableView) -> Int {
              
              return 1
          }

           func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
              
              return 5
            
          }

          
           func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SegundaTableViewCell

              // Configure the cell...

              if indexPath.row == 0 {
                
                cell.nameUSer1.text = "Alberto Barragan"
                cell.imgFlecha.image = UIImage(named: "icon_flechaabajo")
                cell.labelDebe.text = "Debe a"
                cell.labelCantidad.text = "100.00"
                cell.userName2.text = "Luis Gerardo Palacios"
              
              } else if indexPath.row == 1 {
             
              
                cell.nameUSer1.text = "Bety Luna"
                cell.imgFlecha.image = UIImage(named: "icon_flechaabajo")
                cell.labelDebe.text = "Debe a"
                cell.labelCantidad.text = "100.00"
                cell.userName2.text = "Carlos Robles"
              
              
             } else if indexPath.row == 2 {
             
             cell.nameUSer1.text = "Arturo Ceron"
              cell.imgFlecha.image = UIImage(named: "icon_flechaabajo")
              cell.labelDebe.text = "Debe a"
              cell.labelCantidad.text = "100.00"
              cell.userName2.text = "Reyanaldo Escobar"
            
            } else if indexPath.row == 3 {
            
             cell.nameUSer1.text = "Luis Antonio"
             cell.imgFlecha.image = UIImage(named: "icon_flechaabajo")
             cell.labelDebe.text = "Debe a"
             cell.labelCantidad.text = "100.00"
             cell.userName2.text = "Carlos Vazquez"
            
            } else if indexPath.row == 4 {
            
             cell.nameUSer1.text = "Mary Huerta "
             cell.imgFlecha.image = UIImage(named: "icon_flechaabajo")
             cell.labelDebe.text = "Debe a"
             cell.labelCantidad.text = "100.00"
             cell.userName2.text = "Lorena Reyes"
            
            } 
            return cell
          }
          
        



}
