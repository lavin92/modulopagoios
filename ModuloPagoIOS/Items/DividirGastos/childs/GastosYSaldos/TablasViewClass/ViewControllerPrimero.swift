//
//  ViewControllerPrimero.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 13/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ViewControllerPrimero: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView1: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

   // MARK: - Table view data source

      func numberOfSections(in tableView: UITableView) -> Int {
          
          return 1
      }

       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
          return 5
        
      }

      
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PrimeraTableViewCell

          // Configure the cell...

          if indexPath.row == 0 {
            cell.labelFecha.text = "17 de Junio 2020"
            cell.imgProducto.image = UIImage(named: "icon_zapato")
            cell.labelNomArt.text = "Zapato"
            cell.labelCliente.text = "Pagada por YO"
            cell.labelPrecio.text = "$400.00"
            
            
          
          } else if indexPath.row == 1 {
         
          
            cell.labelFecha.text = "16 de Junio 2020"
            cell.imgProducto.image = UIImage(named: "icon_Desayuno")
            cell.labelNomArt.text = "Desayuno"
            cell.labelCliente.text = "Pagada por YO"
            cell.labelPrecio.text = "$150.00"
          
          
         } else if indexPath.row == 2 {
         
         cell.labelFecha.text = "13 de Junio 2020"
         cell.imgProducto.image = UIImage(named: "icon_cafe")
         cell.labelNomArt.text = "Cafe"
         cell.labelCliente.text = "Pagada por YO"
         cell.labelPrecio.text = "$40.00"
        
        } else if indexPath.row == 3 {
         cell.labelFecha.text = "12 de Junio 2020"
         cell.imgProducto.image = UIImage(named: "icon_super")
         cell.labelNomArt.text = "Super"
         cell.labelCliente.text = "Pagada por YO"
         cell.labelPrecio.text = "$1000.00"
        
        } else if indexPath.row == 4 {
         cell.labelFecha.text = "09 de Junio 2020"
         cell.imgProducto.image = UIImage(named: "icon_Transporte")
         cell.labelNomArt.text = "Transporte"
         cell.labelCliente.text = "Pagada por YO"
         cell.labelPrecio.text = "$500.00"
        
        }
        return cell
      }
      
    
}
