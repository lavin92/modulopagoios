//
//  SaldosYGastosVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 11/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class SaldosYGastosVC: UIViewController {

    @IBOutlet weak var containerFirst: UIView!
    
    @IBOutlet weak var containerSecond: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    @IBAction func showContainer(_ sender: UISegmentedControl) {
        
        if(sender.selectedSegmentIndex == 0)
        {
            UIView.animate(withDuration: 0.5, animations:
                {
                    self.containerFirst.alpha = 1.0
                    self.containerSecond.alpha = 0.0
                    
            })
        }
        
        else
        {
            UIView.animate(withDuration: 0.5, animations:
                {
                    self.containerFirst.alpha = 0.0
                    self.containerSecond.alpha = 1.0
            })
        }
        
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   
    
}
