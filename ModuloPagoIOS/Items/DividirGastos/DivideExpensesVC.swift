//
//  DivideExpensesVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 03/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import DLRadioButton

class DivideExpensesVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var textFieldMonto: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewMontoPago: UIView!
    @IBOutlet weak var viewUser: UIView!
    @IBOutlet weak var viewSelectPeople: UIView!
    @IBOutlet weak var dlRButton: DLRadioButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dlRButton.isMultipleSelectionEnabled = true
        
        setUpView()
    }
    
    
    private func setUpView(){
       
        viewMontoPago.layer.cornerRadius = 10
        viewMontoPago.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewMontoPago.layer.shadowOpacity = 0.5
        viewMontoPago.layer.shadowRadius = 4
        viewMontoPago.clipsToBounds = false
        viewMontoPago.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        
        
        
        viewUser.layer.cornerRadius = 10
        viewUser.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewUser.layer.shadowOpacity = 0.5
        viewUser.layer.shadowRadius = 4
        viewUser.clipsToBounds = false
        viewUser.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        
        
        
        viewSelectPeople.layer.cornerRadius = 10
        viewSelectPeople.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewSelectPeople.layer.shadowOpacity = 0.5
        viewSelectPeople.layer.shadowRadius = 4
        viewSelectPeople.clipsToBounds = false
        viewSelectPeople.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let DestViewController : PersonasADividirVC = segue.destination as! PersonasADividirVC
        DestViewController.LabelText = textFieldMonto.text!
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
                   return .lightContent
               }
    
    

      // MARK: - Table view data source

      func numberOfSections(in tableView: UITableView) -> Int {
          
          return 1
      }

       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
          return 11
        
      }

      
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCellPersonas

          // Configure the cell...

          if indexPath.row == 0 {
            
            cell.imgUser.image = UIImage(named: "user_1")
            cell.labelUserName.text = "Alberto Barragan"
            //cell.img_Selectgree.image = UIImage(named: "icon_select")
            cell.imgStart.image = UIImage(named: "icon_start")
              
              
          
          } else if indexPath.row == 1 {
         
          cell.imgUser.image = UIImage(named: "user_3")
          cell.labelUserName.text = "Bety Luna"
          //cell.img_Selectgree.image = UIImage(named: "icon_select")
          cell.imgStart.image = UIImage(named: "icon_start")
            
          
          
         } else if indexPath.row == 2 {
         
          cell.imgUser.image = UIImage(named: "user_2")
          cell.labelUserName.text = "Julian Dorantes"
          //cell.img_Selectgree.image = UIImage(named: "icon_select")
          //cell.imgStart.image = UIImage(named: "icon_start")
        
        } else if indexPath.row == 3 {
        
         cell.imgUser.image = UIImage(named: "user_6")
         cell.labelUserName.text = "Daniela Diaz"
         //cell.img_Selectgree.image = UIImage(named: "icon_select")
         //cell.imgStart.image = UIImage(named: "icon_start")
        
        } else if indexPath.row == 4 {
        
         cell.imgUser.image = UIImage(named: "user_5")
         cell.labelUserName.text = "Clauida Escobar"
         //cell.img_Selectgree.image = UIImage(named: "icon_select")
         //cell.imgStart.image = UIImage(named: "icon_start")
        
        } else if indexPath.row == 5 {
        
         cell.imgUser.image = UIImage(named: "user_4")
         cell.labelUserName.text = "Juan Ceron"
         //cell.img_Selectgree.image = UIImage(named: "icon_select")
         cell.imgStart.image = UIImage(named: "icon_start")
        
        } else if indexPath.row == 6 {
        
         cell.imgUser.image = UIImage(named: "user_7")
         cell.labelUserName.text = "Carlos Vazquez"
         //cell.img_Selectgree.image = UIImage(named: "icon_select")
         //cell.imgStart.image = UIImage(named: "icon_start")
        
        } else if indexPath.row == 7 {
        
         cell.imgUser.image = UIImage(named: "user_6")
         cell.labelUserName.text = "Carlota Venega"
         //cell.img_Selectgree.image = UIImage(named: "icon_select")
         //cell.imgStart.image = UIImage(named: "icon_start")
        
        } else if indexPath.row == 8 {
        
         cell.imgUser.image = UIImage(named: "user_4")
         cell.labelUserName.text = "Roberto Lopez"
         //cell.img_Selectgree.image = UIImage(named: "icon_select")
         //cell.imgStart.image = UIImage(named: "icon_start")
        
        } else if indexPath.row == 9 {
        
         cell.imgUser.image = UIImage(named: "user_6")
         cell.labelUserName.text = "Lorena Sanchez"
         //cell.img_Selectgree.image = UIImage(named: "icon_select")
         //cell.imgStart.image = UIImage(named: "icon_start")
        
        } else if indexPath.row == 10 {
        
         cell.imgUser.image = UIImage(named: "user_4")
         cell.labelUserName.text = "Paco Estrada"
         //cell.img_Selectgree.image = UIImage(named: "icon_select")
         //cell.imgStart.image = UIImage(named: "icon_start")
        
        } else if indexPath.row == 11 {
        
         cell.imgUser.image = UIImage(named: "user_7")
         cell.labelUserName.text = "Carlos Vazquez"
         //cell.img_Selectgree.image = UIImage(named: "icon_select")
         //cell.imgStart.image = UIImage(named: "icon_start")
        
        }
        return cell
      }
      
    

    @IBAction func clickBack(_ sender: Any) {
        let storyboard = UIStoryboard(name: "DivideExpenses", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "CuentaTotalPlayVC") as! CuentaTotalPlayVC
          present(myVC, animated: true, completion: nil)
    }
   
    @IBAction func clickExit(_ sender: Any) {
         let storyboard = UIStoryboard(name: "DivideExpenses", bundle: nil)
         let myVC = storyboard.instantiateViewController(withIdentifier: "CuentaTotalPlayVC") as! CuentaTotalPlayVC
           present(myVC, animated: true, completion: nil)
     }
    
}

