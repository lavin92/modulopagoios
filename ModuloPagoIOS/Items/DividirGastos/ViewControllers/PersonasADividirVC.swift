//
//  PersonasADividirVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 10/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import DLRadioButton

class PersonasADividirVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var dlRButton: DLRadioButton!
    @IBOutlet weak var labelMontoRepartir: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewMontoPago: UIView!
    @IBOutlet weak var viewUser: UIView!
    @IBOutlet weak var viewSelectPeople: UIView!
    
    var LabelText = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dlRButton.isMultipleSelectionEnabled = true
        labelMontoRepartir.text = LabelText
        setUpView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
                   return .lightContent
               }
    
    private func setUpView(){

         viewMontoPago.layer.cornerRadius = 10
         viewMontoPago.layer.shadowOffset = CGSize(width: 5, height: 5)
         viewMontoPago.layer.shadowOpacity = 0.5
         viewMontoPago.layer.shadowRadius = 4
         viewMontoPago.clipsToBounds = false
         viewMontoPago.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
         
         
         
         viewUser.layer.cornerRadius = 10
         viewUser.layer.shadowOffset = CGSize(width: 5, height: 5)
         viewUser.layer.shadowOpacity = 0.5
         viewUser.layer.shadowRadius = 4
         viewUser.clipsToBounds = false
         viewUser.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
         
         
         
         viewSelectPeople.layer.cornerRadius = 10
         viewSelectPeople.layer.shadowOffset = CGSize(width: 5, height: 5)
         viewSelectPeople.layer.shadowOpacity = 0.5
         viewSelectPeople.layer.shadowRadius = 4
         viewSelectPeople.clipsToBounds = false
         viewSelectPeople.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor

        
    }
    
    

      // MARK: - Table view data source

      func numberOfSections(in tableView: UITableView) -> Int {
          
          return 1
      }

       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          
          return 4
        
      }

      
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomCells

          // Configure the cell...

          if indexPath.row == 0 {
            
            cell.imgUser.image = UIImage(named: "user_1")
            cell.labelUserName.text = "Alberto Barragan"
            cell.img_Selectgree.image = UIImage(named: "icon_select")
            cell.moneyCantidad.text = "$100.00"
              
              
          
          } else if indexPath.row == 1 {
         
          cell.imgUser.image = UIImage(named: "user_3")
          cell.labelUserName.text = "Bety Luna"
          cell.img_Selectgree.image = UIImage(named: "icon_select")
          cell.moneyCantidad.text = "$100.00"
            
          
          
         } else if indexPath.row == 2 {
         
          cell.imgUser.image = UIImage(named: "user_2")
          cell.labelUserName.text = "Julian Dorantes"
          cell.img_Selectgree.image = UIImage(named: "icon_select")
          cell.moneyCantidad.text = "$100.00"
        
        } else if indexPath.row == 3 {
        
         cell.imgUser.image = UIImage(named: "user_6")
         cell.labelUserName.text = "Daniela Diaz"
         cell.img_Selectgree.image = UIImage(named: "icon_select")
         cell.moneyCantidad.text = "$100.00"
        
        }
         
        
        

          return cell
      }
      
    

    
    /*@IBAction func clickBackButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "DivideExpenses", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PaymentModuleVC") as! PaymentModuleVC
          present(myVC, animated: true, completion: nil)
    }
*/
    
    

    
}
