//
//  FirstTableViewCell.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 10/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class FirstTableViewCell: UITableViewCell {

    @IBOutlet weak var imgUser1: UIImageView!
    @IBOutlet weak var labelUserName1: UILabel!
    //@IBOutlet weak var img_Selectgree: UIImageView!
    @IBOutlet weak var imgStart1: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
