//
//  SecondTableViewCell.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 10/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class SecondTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgUser2: UIImageView!
    @IBOutlet weak var labelUserName2: UILabel!
    @IBOutlet weak var img_Selectgree2: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
