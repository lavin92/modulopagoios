//
//  CompartirMontoVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 10/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class CompartirMontoVC: UIViewController {

    @IBOutlet weak var viewParentesco: UIView!
    @IBOutlet weak var viweLimiteSaldo: UIView!
    @IBOutlet weak var viewRecurrencia: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()

        
    }
    
    private func setUpView(){
        
        viewParentesco.layer.cornerRadius = 10
        viewParentesco.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewParentesco.layer.shadowOpacity = 0.5
        viewParentesco.layer.shadowRadius = 4
        viewParentesco.clipsToBounds = false
        viewParentesco.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        
        viweLimiteSaldo.layer.cornerRadius = 10
        viweLimiteSaldo.layer.shadowOffset = CGSize(width: 5, height: 5)
        viweLimiteSaldo.layer.shadowOpacity = 0.5
        viweLimiteSaldo.layer.shadowRadius = 4
        viweLimiteSaldo.clipsToBounds = false
        viweLimiteSaldo.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        
        viewRecurrencia.layer.cornerRadius = 10
        viewRecurrencia.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewRecurrencia.layer.shadowOpacity = 0.5
        viewRecurrencia.layer.shadowRadius = 4
        viewRecurrencia.clipsToBounds = false
        viewRecurrencia.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        
        
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
}
