//
//  SaldoCompartidoVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 10/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class SaldoCompartidoVC: UIViewController {

    @IBOutlet weak var viewSaldoExitoso: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setUpView(){
        
        viewSaldoExitoso.layer.cornerRadius = 10
        viewSaldoExitoso.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewSaldoExitoso.layer.shadowOpacity = 0.5
        viewSaldoExitoso.layer.shadowRadius = 4
        viewSaldoExitoso.clipsToBounds = false
        viewSaldoExitoso.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        

    }
    
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func sharePressed(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: ["www.Totalplay.com"], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        
        self.present(activityVC, animated: true, completion: nil)
    }

}
