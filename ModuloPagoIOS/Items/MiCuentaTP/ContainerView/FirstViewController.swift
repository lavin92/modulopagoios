//
//  FirstViewController.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 10/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit


class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewFirst: UITableView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    

     // MARK: - Table view data source

         func numberOfSections(in tableView: UITableView) -> Int {
             
             return 1
         }

          func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             
             return 5
           
         }

         
          func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
             let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! FirstTableViewCell

             // Configure the cell...

             if indexPath.row == 0 {
               
               cell.imgUser1.image = UIImage(named: "user_1")
               cell.labelUserName1.text = "Alberto Barragan"
               //cell.img_Selectgree.image = UIImage(named: "icon_select")
               cell.imgStart1.image = UIImage(named: "icon_start")
                 
                 
             
             } else if indexPath.row == 1 {
            
             cell.imgUser1.image = UIImage(named: "user_3")
             cell.labelUserName1.text = "Bety Luna"
             //cell.img_Selectgree.image = UIImage(named: "icon_select")
             cell.imgStart1.image = UIImage(named: "icon_start")
               
             
             
            } else if indexPath.row == 2 {
            
             cell.imgUser1.image = UIImage(named: "user_2")
             cell.labelUserName1.text = "Julian Dorantes"
             //cell.img_Selectgree.image = UIImage(named: "icon_select")
             cell.imgStart1.image = UIImage(named: "icon_start")
           
           } else if indexPath.row == 3 {
           
            cell.imgUser1.image = UIImage(named: "user_6")
            cell.labelUserName1.text = "Daniela Diaz"
            //cell.img_Selectgree.image = UIImage(named: "icon_select")
            cell.imgStart1.image = UIImage(named: "icon_start")
           
           } else if indexPath.row == 4 {
           
            cell.imgUser1.image = UIImage(named: "user_5")
            cell.labelUserName1.text = "Clauida Escobar"
            //cell.img_Selectgree.image = UIImage(named: "icon_select")
            cell.imgStart1.image = UIImage(named: "icon_start")
           
           } else if indexPath.row == 5 {
           
            cell.imgUser1.image = UIImage(named: "user_4")
            cell.labelUserName1.text = "Juan Ceron"
            //cell.img_Selectgree.image = UIImage(named: "icon_select")
            cell.imgStart1.image = UIImage(named: "icon_start")
           
           } else if indexPath.row == 6 {
           
            cell.imgUser1.image = UIImage(named: "user_7")
            cell.labelUserName1.text = "Carlos Vazquez"
            //cell.img_Selectgree.image = UIImage(named: "icon_select")
            cell.imgStart1.image = UIImage(named: "icon_start")
           
           }
           
           

             return cell
         }
         
}
