//
//  HisotoryDataSource.swift
//  ModuloPagoIOS
//
//  Created by Charls Salazar on 17/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class HisotoryDataSource: NSObject, UITableViewDataSource, UITableViewDelegate  {
    var mSectionsArray : [HistoryMovements] = []
    var tableView : UITableView?
    var mReferenceItem : String = "ItemMovimientosTableViewCell"
    var mViewController:  UIViewController?
    
    init(tableView: UITableView, mViewController:  UIViewController) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.estimatedRowHeight = 10.0
        self.tableView?.rowHeight = UITableView.automaticDimension
        let nib = UINib(nibName: mReferenceItem, bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: mReferenceItem)
        self.mViewController = mViewController
    }
    
    func update(items: [HistoryMovements]) {
        self.mSectionsArray = items
        self.tableView?.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mSectionsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.mSectionsArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: mReferenceItem) as! ItemMovimientosTableViewCell

        cell.mAddressLabel.text = item.mTradeType
        cell.mTitleMovementLabel.text = item.mOrderTitle
        cell.mAmmountMovementLabel.text = "$" + item.mTotalAmount!
        cell.mDateMovementLabel.text = item.mPaySuccessTime
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
