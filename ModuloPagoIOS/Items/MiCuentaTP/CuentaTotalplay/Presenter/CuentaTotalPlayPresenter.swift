
//
//  CuentaTotalPlayPresenter.swift
//  ModuloPagoIOS
//
//  Created by Charls Salazar on 20/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

protocol HistoryDelegate : NSObjectProtocol {
    func getHistory(mListMovements : [HistoryMovements])
    func getBalance(mBalance : String)
}

class CuentaTotalPlayPresenter: ModuloPagoPresenter{
    var mViewController : UIViewController!
    var mHistoryMovementsDelegate : HistoryDelegate!
    var mURL : String = ""
    
    init(mViewController : UIViewController, mHistoryMovementsDelegate : HistoryDelegate) {
        super.init()
        self.mViewController = mViewController
        self.mHistoryMovementsDelegate = mHistoryMovementsDelegate
    }
    
    func getHistoryAccount(mIdAccount : String){
        let mBizContent = BizzContentHistory(mIdentifier: "8615000000218",
                                             mIdentifierType: "01",
                                             mMerch_code: "",
                                             mType: "1000",
                                             mOffset: "1",
                                             mLimit: "10",
                                             mStartTime: "2020-07-06 15:00:00",
                                             mEndTime: "2020-07-14 15:00:00")
        
        let mRequest = HistoryMovementsRequets(mTimeStamp: "1535166225",
                                               mNonceStr: "5K8264ILTKCH16CQ2502SI8ZNMTM67VS",
                                               mMethod: "payment.queryhistory",
                                               mSignType: "HmacSHA256",
                                               mSign: "8ed50aa94c1db91e8a765566caf3a46936db4ab5677683978f7d221e74a052e3",
                                               mVersion: "1.0",
                                               mAppCode: "398972237158402",
                                               mBizContentHistory: mBizContent)
        
        RetrofitManager<HistoryMovementsResponse>.init(requestUrl: Definitions.WS_HISTORY, delegate: self).request(requestModel: mRequest)
        AlertDialog.showOverlay()
    
    }
    
    func checkBalance(){
       mURL = "https://alonsoserrano-eval-test.apigee.net/huawei/payment/v1/consumers/" + Constantes.mIDPayer + "/accounts?identifierType=01"
        RetrofitManager<QueryCustomerBalanceResponse>.init(requestUrl: mURL, delegate: self).requestGet()
        AlertDialog.showOverlay()
           
    }
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        switch requestUrl {
        case Definitions.WS_HISTORY:
            OnGetHistorySuccess(mResponse: response as! HistoryMovementsResponse)
            break
            
        case mURL:
            onGetBalance(mResponse: response as! QueryCustomerBalanceResponse)
            break
        default:
            break
        }
    }
    
    func onGetBalance(mResponse : QueryCustomerBalanceResponse){
        if(mResponse.mResultCode == "0"){
            if(mResponse.mAccounBalanceList.count > 0){
                mHistoryMovementsDelegate.getBalance(mBalance: mResponse.mAccounBalanceList[0].mCurrentBalance!)
            }
        }
        AlertDialog.hideOverlay()
    }
    
    func OnGetHistorySuccess(mResponse : HistoryMovementsResponse){
        if(mResponse.mCode == "0"){
            if(mResponse.mBizContentHistory != nil){
                if(mResponse.mBizContentHistory?.mHistory.count != nil){
                    mHistoryMovementsDelegate.getHistory(mListMovements: mResponse.mBizContentHistory!.mHistory)
                }
            }
        }
        checkBalance()
    }
    
    override func onErrorConnection() {
        AlertDialog.hideOverlay()
    }
    
    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        AlertDialog.hideOverlay()
    }
}
