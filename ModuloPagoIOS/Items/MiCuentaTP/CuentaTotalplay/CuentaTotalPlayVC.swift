//
//  CuentaTotalPlayVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 13/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class CuentaTotalPlayVC: BaseViewController, HistoryDelegate, SwipeableViewProtocol{
    
    @IBOutlet weak var mMainContainerView: SwipeableView!
    @IBOutlet weak var mBackMovementsTableView: UITableView!
    var mCuentasPresenter : CuentaTotalPlayPresenter!
    var mHistoryDataSource : HisotoryDataSource!
    
    @IBOutlet weak var mBalanceLabel: UILabel!
    @IBOutlet weak var mNameLabel: UILabel!
    
    @IBOutlet weak var mScrollView: UIScrollView!
    @IBOutlet weak var mTopDetailTopConstraint: NSLayoutConstraint!
    private var currencyFormatter: NumberFormatter?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        mHistoryDataSource = HisotoryDataSource(tableView: mBackMovementsTableView, mViewController: self)
        self.mMainContainerView.delegate = self
        self.mMainContainerView.flexibleLayout = .init(with: self.mTopDetailTopConstraint, end: 0)
        
        self.mMainContainerView.hasRoundedCorners = true
        self.mMainContainerView.autoDetectScrollViews = true
        self.mMainContainerView.childViewInteractionOnExpandedOnly = true
        self.mMainContainerView.coordinatedScrollView = mScrollView
        setup()
        mNameLabel.text = selectUser()
    }
    
    override func getPresenter() -> BasePresenter? {
        mCuentasPresenter = CuentaTotalPlayPresenter(mViewController: self,mHistoryMovementsDelegate: self)
        return mCuentasPresenter
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mCuentasPresenter.getHistoryAccount(mIdAccount: "8615000000218")
    }
    
    func selectUser()->String{
        switch Constantes.mIDPayer {
        case "8615000000218":
            return "Aurelio"
            
        case "8615000000053":
            return "Eduardo"
            
        case "100002303":
            return "Doña Blanca"
            
        case "100002301":
            return "Crepe Time"
        default:
            return ""
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickBack(_ sender: Any) {
        let storyboard = UIStoryboard(name: "CuentaTotalPlay", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PaymentModuleVC") as! PaymentModuleVC
        present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func clickCompartirFamAmigos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "CuentaTotalPlay", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "MyAccountTPVC1") as! MyAccountTPVC1
        present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func clickPagar(_ sender: Any) {
        let storyboard = UIStoryboard(name: "CuentaTotalPlay", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PayVC") as! PayVC
        present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func clickCobrar(_ sender: Any) {
        let storyboard = UIStoryboard(name: "CuentaTotalPlay", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "CollectVC") as! CollectVC
        present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func clickDividir(_ sender: Any) {
        let storyboard = UIStoryboard(name: "CuentaTotalPlay", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "DivideExpensesVC") as! DivideExpensesVC
        present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func clickPantallaPrincial(_ sender: Any) {
        let storyboard = UIStoryboard(name: "CuentaTotalPlay", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PaymentModuleVC") as! PaymentModuleVC
        present(myVC, animated: true, completion: nil)
    }
    
    func getHistory(mListMovements: [HistoryMovements]) {
        mHistoryDataSource.update(items: mListMovements)
    }
    
    func getBalance(mBalance: String) {
        mBalanceLabel.text = (self.currencyFormatter?.string(from: NSNumber(value: Double(mBalance)!)))!
    }
    
    
    func swipeableViewDidExpand(swipeableView: SwipeableView, previousState: Bool) {
        
    }
    
    func swipeableViewDidCollapse(swipeableView: SwipeableView, previousState: Bool) {
        
    }
    
    func swipeableViewDidPan(swipeableView: SwipeableView, percentage: CGFloat) {
        
    }
    
    private func setup() {
           currencyFormatter = NumberFormatter()
           currencyFormatter!.usesGroupingSeparator = true
           currencyFormatter!.numberStyle = .currency
           currencyFormatter!.locale = Locale.current
    }
       
    
}
