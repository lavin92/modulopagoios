//
//  PayAmountVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 07/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import LocalAuthentication

class PayAmountVC: BaseViewController, PaySuccessDelegate {

    @IBOutlet weak var viewPagar: UIView!
    @IBOutlet weak var textFieldMontoAPagar: UITextField!
    //variables para facetouchID
    let mycontext: LAContext = LAContext()
    var error: NSError?
    
    var mIDPayee : String!
    var mToken : String!
    var mPayAmountPresenter : PayAmountPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewPagar.layer.cornerRadius = 10
        viewPagar.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewPagar.layer.shadowOpacity = 0.5
        viewPagar.layer.shadowRadius = 4
        viewPagar.clipsToBounds = false
        viewPagar.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        
    }
    @IBAction func mPayButton(_ sender: Any) {
        //Pay()
        
        
        //DatosBiométricos
        if mycontext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                   let reason = "Reconocimiento Biométrico"

           mycontext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { [weak self] success, authenticationError in DispatchQueue.main.async
               {
               if success {
                self?.Pay()
               } else {
               // error
               }
                }
                   }
               
               } else {
                   // no biometry
               }
        
    }
    
    override func getPresenter() -> BasePresenter? {
        mPayAmountPresenter = PayAmountPresenter(mViewController: self, mPaySuccessDelegate: self)
        return mPayAmountPresenter
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let DestViewController : SuccessfulPaymentVC = segue.destination as! SuccessfulPaymentVC
        DestViewController.LabelText = textFieldMontoAPagar.text!
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func verifyTypeTransaction(){
        let index = mIDPayee.index(mIDPayee.startIndex, offsetBy: 2)
        let mySubstring = mIDPayee[..<index]
        
        if(mySubstring == "86"){
            mPayAmountPresenter.payAmountP2P(mIdPayee: mIDPayee, mAmmount: textFieldMontoAPagar.text!, mToken:  mToken)
        }else{
            mPayAmountPresenter.payAmountC2B(mIdPayee: mIDPayee, mAmmount: textFieldMontoAPagar.text!,mToken: mToken)

        }
    }
    
    func Pay(){
        if(textFieldMontoAPagar.text != "" || textFieldMontoAPagar.text != nil){
            verifyTypeTransaction()
        }else{
            AlertDialog.show(title: "Aviso", body: "Es necesario ingresar el monto", view: self)
        }
    }
    
    func OnSuccessPay() {
        let storyboard = UIStoryboard(name: "PayAmount", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "SuccessfulPaymentVC") as! SuccessfulPaymentVC
        myVC.mAmmount = textFieldMontoAPagar.text!
        myVC.mIdPayee = mIDPayee
        present(myVC, animated: true, completion: nil)
        
    }
    
    /*@IBAction func clickCantidadAPagar(_ sender: Any) {
     let storyboard = UIStoryboard(name: "PayAmount", bundle: nil)
     let myVC = storyboard.instantiateViewController(withIdentifier: "SuccessfulPaymentVC") as! SuccessfulPaymentVC
     present(myVC, animated: true, completion: nil)
     }*/
    
    @IBAction func clickButtonExit(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PayAmount", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PayVC") as! PayVC
        present(myVC, animated: true, completion: nil)
    }
    
    
    
}
