//
//  PayAmountPresenter.swift
//  ModuloPagoIOS
//
//  Created by Charls Salazar on 20/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import Firebase

protocol PaySuccessDelegate : NSObjectProtocol {
    func OnSuccessPay()
}

class PayAmountPresenter: ModuloPagoPresenter{
    var mIdPayee : String = ""
    var mAmmount : String = ""
    var mViewController : UIViewController!
    var mPaySuccessDelegate : PaySuccessDelegate!
    lazy var functions = Functions.functions()
    var mToken : String!
    
    init(mViewController : UIViewController, mPaySuccessDelegate : PaySuccessDelegate) {
        super.init()
        self.mViewController = mViewController
        self.mPaySuccessDelegate = mPaySuccessDelegate
    }
    
    func payAmountP2P(mIdPayee : String, mAmmount: String , mToken : String){
        self.mIdPayee = mIdPayee
        self.mAmmount = mAmmount
        self.mToken = mToken
        
        let mBizContentRequest = BizContent(mAppid: "398972237158402",
                                            mMerchCode: "100002303",
                                            mMerchOrderId: getRandomString(length:15),
                                            mPayerIdentifierType: "01",
                                            mPayerIdentifier: Constantes.mIDPayer,
                                            mPayerType: "1000",
                                            mPayeeIdentifierType: "01",
                                            mPayeeIdentifier: mIdPayee ,
                                            mPayeeType: "1000",
                                            mInitiatorIdentifierType: "01",
                                            mInitiatorIdentifier: Constantes.mIDPayer,
                                            mInitiatorType: "1000",
                                            mSecurityCredential: "bE9+T9C4V9vEv0w6K9qkLnLcxTBuKDGkpkIAySe8o3ebDkY3338+qxYTmVRVLM6XRxVDSYJPd0PJzb8jWOPK4zHKlxZNGivK1+uniR1BgTntGRmk5gjR8TOTLeU1EUsdyOG84InDzNdHYsKoMuMM3G3x3E7lSf8uRcmvHxYynXlpGt1W6TlpkmG8UWUPMNd46vC0Tv7D9Dan81EtuOHPuHl66q02vFV5fVVinWcURIX84s83x3XHQ9NUpZ5/7LfJNzchc3tWHd8aAfH+XpSZVf2X1S3W0cMHAHSaLK0RO8goWWwkHyVcHaJb8Nto34R15fZ5ZK+XeadsR+Kb8+R5nA==",
                                            mTradeType: "NativeApp",
                                            mTitle: "GameRecharge",
                                            mTotalAmount: mAmmount,
                                            mTransCurrency: "USD",
                                            mTimeoutExpress: "100m",
                                            mBusinessType: "P2PTransfer",
                                            mCallbackInfo: "string"
                                            )
        
        let mCreatePayOrderP2PRequest = CreatePayOrderP2PRequest(mTimeStamp: "1563161657",
                                                                 mMethod: "payment.preorder",
                                                                 mNoceStr: "5K8264ILTKCH16CQ2502SI8ZNMTM67VS",
                                                                 mSignType: "HmacSHA256",
                                                                 mSign: "BC4EE8D710BAC6A7E33DE4511A1CE7723024615EEF491B80DEF7DC743D4DADBE",
                                                                 mVersion: "1.0",
                                                                 mBizContent: mBizContentRequest)
        
        
        RetrofitManager<CreatePayOrderP2PResponse>.init(requestUrl: Definitions.WS_CREATE_PAY, delegate: self).request(requestModel: mCreatePayOrderP2PRequest)
        AlertDialog.showOverlay()
    }
    
    func payAmountC2B(mIdPayee : String, mAmmount: String, mToken : String){
        self.mIdPayee = mIdPayee
        self.mAmmount = mAmmount
        self.mToken = mToken
               
        let mBizContentCB = BizContentCB(mNotifyUrl: "http://test.payment.com/notify",
                                         mRedirectUrl: "http://test.payment.com/redirect",
                                         mAppid: "398972237158402",
                                         mMerchCode: mIdPayee,
                                         mMerchOrderId: getRandomString(length: 15),
                                         mPayerIdentifierType: "01",
                                         mPayerIdentifier: Constantes.mIDPayer,
                                         mPayerType: "1000",
                                         mPayeeIdentifierType: "04",
                                         mPayeeIdentifier: mIdPayee,
                                         mPayeeType: "5000",
                                         mInitiatorIdentifierType: "01",
                                         mInitiatorIdentifier: Constantes.mIDPayer,
                                         mInitiatorType: "1000",
                                         mInitiatorMerchCode: "",
                                         mSecurityCredential: "bE9+T9C4V9vEv0w6K9qkLnLcxTBuKDGkpkIAySe8o3ebDkY3338+qxYTmVRVLM6XRxVDSYJPd0PJzb8jWOPK4zHKlxZNGivK1+uniR1BgTntGRmk5gjR8TOTLeU1EUsdyOG84InDzNdHYsKoMuMM3G3x3E7lSf8uRcmvHxYynXlpGt1W6TlpkmG8UWUPMNd46vC0Tv7D9Dan81EtuOHPuHl66q02vFV5fVVinWcURIX84s83x3XHQ9NUpZ5/7LfJNzchc3tWHd8aAfH+XpSZVf2X1S3W0cMHAHSaLK0RO8goWWwkHyVcHaJb8Nto34R15fZ5ZK+XeadsR+Kb8+R5nA==",
                                         mTradeType: "QrCode",
                                         mTitle: "GameRecharge",
                                         mTotalAmount: mAmmount,
                                         mTransCurrency: "USD",
                                         mTimeoutExpress: "100m",
                                         mBusinessType: "BuyGoods",
                                         mCallbackInfo: "string")
        
        let mCreatePayOrderC2BRequest = CreatePayOrderC2BRequest(mTimeStamp: "1563161657",
                                                                 mMethod: "payment.preorder",
                                                                 mNoceStr: "5K8264ILTKCH16CQ2502SI8ZNMTM67VS",
                                                                 mSignType: "HmacSHA256",
                                                                 mSign: "BC4EE8D710BAC6A7E33DE4511A1CE7723024615EEF491B80DEF7DC743D4DADBE",
                                                                 mVersion: "1.0",
                                                                 mBizContentCB: mBizContentCB)
    
        RetrofitManager<CreatePayOrderC2BResponse>.init(requestUrl: Definitions.WS_CREATE_PAY, delegate: self).request(requestModel: mCreatePayOrderC2BRequest)
        AlertDialog.showOverlay()
    }
    
    override func onSuccessLoadResponse(requestUrl: String, response: BaseResponse) {
        let indexFrom = Constantes.mIDPayer.index(Constantes.mIDPayer.startIndex, offsetBy: 2)
        let indexEnd = mIdPayee.index(mIdPayee.startIndex, offsetBy: 2)
        
        let mIdFromPayee = Constantes.mIDPayer[..<indexFrom]
        let mIDEndPayee = mIdPayee[..<indexEnd]
        if(requestUrl == Definitions.WS_CREATE_PAY){
            if(mIdFromPayee == "86" && mIDEndPayee == "86" ){
                OnSuccessPayP2P(mResponse: response as! CreatePayOrderP2PResponse)
            }else{
                OnSuccessPayC2B(mResponse: response as! CreatePayOrderC2BResponse)
            }
        }
    }
    
    
    func sendNotification(){
        let data = ["sender_name": selectUser(),
            "amount": mAmmount,
            "device_token": mToken]
           functions.httpsCallable("sendNotification").call(data) { (result, error) in
             // [START function_error]
             if let error = error as NSError? {
               if error.domain == FunctionsErrorDomain {
                 let code = FunctionsErrorCode(rawValue: error.code)
                 let message = error.localizedDescription
                 let details = error.userInfo[FunctionsErrorDetailsKey]
               }
               // [START_EXCLUDE]
               print(error.localizedDescription)
               return
               // [END_EXCLUDE]
             }
             // [END function_error]
             if let operationResult = (result?.data as? [String: Any])?["operationResult"] as? Int {
               print("####FIREBASE","\(operationResult)")
             }
           }
    }
    
    func OnSuccessPayP2P(mResponse : CreatePayOrderP2PResponse){
        if(mResponse.mCode == "0"){
            sendNotification()
            mPaySuccessDelegate.OnSuccessPay()
        }else{
            AlertDialog.show(title: "Aviso" , body: mResponse.mResult!, view: mViewController)
        }
        AlertDialog.hideOverlay()
    }
    
    func OnSuccessPayC2B(mResponse : CreatePayOrderC2BResponse){
           if(mResponse.mCode == "0"){
               sendNotification()
               mPaySuccessDelegate.OnSuccessPay()
           }else{
               AlertDialog.show(title: "Aviso" , body: mResponse.mResult!, view: mViewController)
           }
           AlertDialog.hideOverlay()
       }
    
    override func onErrorConnection() {
        AlertDialog.hideOverlay()
    }
    
    override func onErrorLoadResponse(requestUrl: String, messageError: String) {
        AlertDialog.hideOverlay()
    }
    
    func getRandomString(length: Int) -> String {
        let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let allowedCharsCount = UInt32(allowedChars.count)
        var randomString = ""

        for _ in 0..<length {
            let randomNum = Int(arc4random_uniform(allowedCharsCount))
            let randomIndex = allowedChars.index(allowedChars.startIndex, offsetBy: randomNum)
            let newCharacter = allowedChars[randomIndex]
            randomString += String(newCharacter)
        }

        return randomString
    }
    
    func selectUser()->String{
        switch Constantes.mIDPayer {
        case "8615000000218":
            return "Aurelio"
            
        case "8615000000053":
            return "Eduardo"
            
        case "100002303":
            return "Doña Blanca"
            
        case "100002301":
            return "Crepe Time"
        default:
            return ""
        }
    }
}
