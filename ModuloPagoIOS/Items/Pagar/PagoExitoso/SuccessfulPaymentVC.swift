//
//  SuccessfulPaymentVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 07/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class SuccessfulPaymentVC: UIViewController {

    @IBOutlet weak var labelFechaPago: UILabel!
    @IBOutlet weak var labelMontoPagado: UILabel!
    @IBOutlet weak var labelNomCuenta: UILabel!
    @IBOutlet weak var labelReferencia: UILabel!
    @IBOutlet weak var labelFolio: UILabel!
    @IBOutlet weak var labelClaveRastreo: UILabel!
    @IBOutlet weak var viewPaymentDetails: UIView!
    @IBOutlet weak var mIdAccountLabel: UILabel!
    
    var LabelText = String()
    var mAmmount : String = ""
    var mIdPayee : String = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setDataSuccess()
    }
    
    
    func setDataSuccess(){
       labelMontoPagado.text = mAmmount + ".00"
        labelNomCuenta.text = selectUser().uppercased()
        labelFechaPago.text = Date().fullDate
        mIdAccountLabel.text = Constantes.mIDPayer
    }
    
     func selectUser()->String{
           switch mIdPayee {
           case "8615000000218":
               return "Aurelio"
               
           case "8615000000053":
               return "Eduardo"
               
           case "100002303":
               return "Doña Blanca"
               
           case "100002301":
               return "Crepe Time"
           default:
               return ""
           }
       }
    
    
    private func setUpView(){
        
        viewPaymentDetails.layer.cornerRadius = 10
        viewPaymentDetails.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewPaymentDetails.layer.shadowOpacity = 0.5
        viewPaymentDetails.layer.shadowRadius = 4
        viewPaymentDetails.clipsToBounds = false
        viewPaymentDetails.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor

    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
              return .lightContent
          }
    
    @IBAction func sharePressed(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: ["www.Totalplay.com"], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    /*@IBAction func clickBackButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PayAmountVC", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PayAmountVC") as! PayAmountVC
          present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func clickButtonExit(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PayAmountVC", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PaymentModuleVC") as! PaymentModuleVC
        
          present(myVC, animated: true, completion: nil)
    }*/
    
}
