//
//  CustomQRViewController.swift
//  ModuloPagoIOS
//
//  Created by Charls Salazar on 20/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import AVFoundation


class CustomQRViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

var captureSession: AVCaptureSession! //
var previewLayer : AVCaptureVideoPreviewLayer!

override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = UIColor.black //
    captureSession = AVCaptureSession() //
    
    guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return } //
    let videoInput: AVCaptureDeviceInput //
    
    do {
        videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
    } catch {
        return
    }
    
    if (captureSession.canAddInput(videoInput)) {
        captureSession.addInput(videoInput)
    }else {
        failed()
        return
    }
    
    let metadataOutput = AVCaptureMetadataOutput()
    
    if (captureSession.canAddOutput(metadataOutput)) {
        captureSession.addOutput(metadataOutput)
        
        metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        metadataOutput.metadataObjectTypes = [.qr]
    } else {
        failed()
        return
    }
    
    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
    previewLayer.frame = view.layer.bounds
    previewLayer.videoGravity = .resizeAspectFill
    view.layer.addSublayer(previewLayer)
        
    captureSession.startRunning()
}

func failed() {
    let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
    ac.addAction(UIAlertAction(title: "OK", style: .default))
    present(ac, animated: true)
    captureSession = nil
}

override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    if (captureSession?.isRunning == false) {
        captureSession.startRunning()
    }
}

override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    if (captureSession?.isRunning == true) {
        captureSession.stopRunning()
    }
}

func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
    //cheking if we have some data to process
    if metadataObjects.count > 0 {
        //casting it to a machine readble code object
        if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
            //Checcking if type id of QR code or not
            if object.type == AVMetadataObject.ObjectType.qr {
                //displaying it as an alert
                var msj = object.stringValue
                //print(msj!)
                //Divide name and number:
                let fullMsj = msj//.components(separatedBy: "/")
                print(fullMsj)
                //let number = fullMsj![0]
                //let name = fullMsj![1]
                //print(number)
                //print(name)
                
                let alert = UIAlertController(title: "QR Code", message: msj, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Retake", style: .default, handler: nil))
                //Present the alert now
                present(alert, animated: true, completion: nil)
                
                //UIApplication.shared.canOpenURL(NSURL(string: "contacts://")! as URL)
                
            }

        }
    }
    /*captureSession.stopRunning()
     
     if let metadataObject = metadataObjects.first {
     guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
     guard let stringValue = readableObject.stringValue else { return }
     AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
     found(code: stringValue)
     }
     
     dismiss(animated: true)*/
}

func found(code: String) {
    print(code)
}

override var prefersStatusBarHidden: Bool {
    return true
}

override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return .portrait
}

}
