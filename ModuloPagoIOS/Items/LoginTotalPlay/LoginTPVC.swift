//
//  LoginTPVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 14/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import Firebase
import LocalAuthentication

class LoginTPVC: UIViewController {

    
    @IBOutlet weak var mViewLogin: UIView!
   
    @IBOutlet weak var loginFaceTouchID: UIButton!
    @IBOutlet weak var labelFaceTouchID: UILabel!
    
    @IBOutlet weak var texFieldNoCuenta: UITextField!
    @IBOutlet weak var texFieldPassword: UITextField!
    
    let mycontext: LAContext = LAContext()
    var error: NSError?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgFaceTouchID()
        setUpView()
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .lightContent
       }
    
    private func setUpView(){
        
        mViewLogin.layer.cornerRadius = 10
        mViewLogin.layer.shadowOffset = CGSize(width: 5, height: 5)
        mViewLogin.layer.shadowOpacity = 0.5
        mViewLogin.layer.shadowRadius = 4
        mViewLogin.clipsToBounds = false
        mViewLogin.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        
    }
    
    private func imgFaceTouchID(){
        
        if
            mycontext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) {
            if mycontext.biometryType == .faceID {
                labelFaceTouchID.text?.append("Ingresa con tu rostro")
                loginFaceTouchID.setImage(UIImage(named: "icon_faceID"), for: UIControl.State.normal)
            } else if mycontext.biometryType == .touchID {
                labelFaceTouchID.text?.append("Ingresa con tu huella")
                loginFaceTouchID.setImage(UIImage(named: "icon_touchID"), for: UIControl.State.normal)
            }
            //Reconocimiento Biométrico
        }
    }
    
    
    
    @IBAction func visiblePasswordButton(_ sender: Any) {
        texFieldPassword.isSecureTextEntry.toggle()
           
       }

    @IBAction func autentitficateButton(_ sender: Any) {
        
    if mycontext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Reconocimiento Biométrico"

    mycontext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { [weak self] success, authenticationError in DispatchQueue.main.async
        {
        if success {
        self?.autenticateUser()
        } else {
        // error
        }
         }
            }
        
        } else {
            // no biometry
        }
       
        
        
    }
    
    
    @IBAction func clickIniciarSesion(_ sender: Any) {
        if(texFieldNoCuenta.text != "" && texFieldNoCuenta.text != nil && texFieldPassword.text != "" && texFieldPassword.text != nil ){
            selectUserFromSesion(mName: texFieldNoCuenta.text!)
        }else{
            AlertDialog.show(title: "Aviso", body: "Es necesario llenar ambos campos", view: self)
        }
        
    }
    
    func selectUserFromSesion(mName: String){
        switch mName {
        case "Aurelio":
            Constantes.mIDPayer = "8615000000218"
            break
            
        case "aurelio":
            Constantes.mIDPayer = "8615000000218"
            break
            
        case "Eduardo":
            Constantes.mIDPayer = "8615000000053"
            break
            
        case "eduardo":
            Constantes.mIDPayer = "8615000000053"
            break
            
        case "Arte Helada":
            Constantes.mIDPayer = "8615000000053"
            break
            
        case "arte Heleda":
            Constantes.mIDPayer = "100002301"
            break
            
        case "Arte helada":
            Constantes.mIDPayer = "100002301"
            break
            
        case "arte helada":
            Constantes.mIDPayer = "100002301"
            break
            
        case "Doña Blanca":
            Constantes.mIDPayer = "100002303"
            break
            
        case "doña Blanca":
            Constantes.mIDPayer = "100002303"
            break
            
        case "Doña blanca":
            Constantes.mIDPayer = "100002303"
            break
            
        case "doña blanca":
            Constantes.mIDPayer = "100002303"
            break
            
        default:
            Constantes.mIDPayer = "8615000000218"
            break
        }
        
        autenticateUser()
    }
    
    func autenticateUser(){
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
             let myVC = storyboard.instantiateViewController(withIdentifier: "PaymentModuleVC") as! PaymentModuleVC
               present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func clickCrearCuenta(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "TradingAccountVC") as! TradingAccountVC
                 present(myVC, animated: true, completion: nil)
    }
    
    func getToken(){
        
    }

    
}
