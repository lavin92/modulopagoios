//
//  PaymentModuleVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 02/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class PaymentModuleVC: UIViewController {

    @IBOutlet weak var viewAccionesPago: UIView!
    @IBOutlet weak var viewPagar: UIView!
    @IBOutlet weak var viewCobrar: UIView!
    @IBOutlet weak var viewDividirGastos: UIView!
    @IBOutlet weak var viewMiCuentaTP: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let view = UIView(); viewAccionesPago.clipsToBounds = true; viewAccionesPago.layer.cornerRadius = 12; viewAccionesPago.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
       
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickButtonPagar(_ sender: Any) {
        
       let storyboard = UIStoryboard(name: "PaymentModule", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PayVC") as! PayVC
          present(myVC, animated: true, completion: nil)
    }
    
    func callQR(){
           let storyboard = UIStoryboard(name: "CustomQR", bundle: nil)
           let myVC = storyboard.instantiateViewController(withIdentifier: "CustomQRViewController") as! CustomQRViewController
           present(myVC, animated: true, completion: nil)
       }
       
    
    
    @IBAction func clickButtonCobrar(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PaymentModule", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "CollectVC") as! CollectVC
          present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func clickDividirGastos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PaymentModule", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "DivideExpensesVC") as! DivideExpensesVC
          present(myVC, animated: true, completion: nil)
    }
    
    
    @IBAction func clickMiCuentaTP(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PaymentModule", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "CuentaTotalPlayVC") as! CuentaTotalPlayVC
          present(myVC, animated: true, completion: nil)
    }
    
    
    
    @IBAction func clickCerrarSesion(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PaymentModule", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "LoginTPVC") as! LoginTPVC
          present(myVC, animated: true, completion: nil)
    }
    
    
}


