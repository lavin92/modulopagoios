//
//  AssociateAccountVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 15/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class AssociateAccountVC: UIViewController {

   @IBOutlet weak var viewSaldo: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    private func setUpView(){
        viewSaldo.layer.cornerRadius = 10
        viewSaldo.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewSaldo.layer.shadowOpacity = 0.5
        viewSaldo.layer.shadowRadius = 4
        viewSaldo.clipsToBounds = false
        viewSaldo.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
    }

    

    @IBAction func onclickBack(_ sender: Any) {
        let storyboard = UIStoryboard(name: "AssociateAccount", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "AccountCreatedVC") as! AccountCreatedVC
          present(myVC, animated: true, completion: nil)
    }
    
   @IBAction func btnPerfil(_ sender: Any) {
        let storyboard = UIStoryboard(name: "AssociateAccount", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ProfileAssociateVC") as! ProfileAssociateVC
          present(myVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnPagarQR(_ sender: Any) {
        let storyboard = UIStoryboard(name: "AssociateAccount", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ColletQRVC") as! ColletQRVC
          present(myVC, animated: true, completion: nil)
    }
    
}
