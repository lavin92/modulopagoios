//
//  BankAccountVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 15/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit
import DLRadioButton

class BankAccountVC: UIViewController {

    @IBOutlet weak var btnNotificacion: DLRadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         btnNotificacion.isMultipleSelectionEnabled = true
    }

  override var preferredStatusBarStyle: UIStatusBarStyle {
      return .lightContent
  }
    
    
    @IBAction func onclickBack(_ sender: Any) {
           let storyboard = UIStoryboard(name: "BankAccount", bundle: nil)
           let myVC = storyboard.instantiateViewController(withIdentifier: "ScanActaVC") as! ScanActaVC
             present(myVC, animated: true, completion: nil)
       }
       
       @IBAction func btnCuentaBanco(_ sender: Any) {
           let storyboard = UIStoryboard(name: "BankAccount", bundle: nil)
           let myVC = storyboard.instantiateViewController(withIdentifier: "ScanCardVC") as! ScanCardVC
             present(myVC, animated: true, completion: nil)
       }
    
}
