//
//  ScanActaVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 15/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ScanActaVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func onclickBack(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ScanActa", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ScanAddressVC") as! ScanAddressVC
          present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func btnScanActa(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ScanActa", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "BankAccountVC") as! BankAccountVC
          present(myVC, animated: true, completion: nil)
    }
    
    
}
