//
//  ScanCardVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 15/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ScanCardVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    
    
    @IBAction func onclickBack(_ sender: Any) {
              let storyboard = UIStoryboard(name: "ScanCard", bundle: nil)
              let myVC = storyboard.instantiateViewController(withIdentifier: "BankAccountVC") as! BankAccountVC
                present(myVC, animated: true, completion: nil)
          }
          
          @IBAction func btnEscanearTarjeta(_ sender: Any) {
              let storyboard = UIStoryboard(name: "ScanCard", bundle: nil)
              let myVC = storyboard.instantiateViewController(withIdentifier: "AccountCreatedVC") as! AccountCreatedVC
                present(myVC, animated: true, completion: nil)
          }
}
