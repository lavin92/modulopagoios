//
//  ScanAddressVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 15/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ScanAddressVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func onClickBack(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ScanAddress", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ScanDocumentVC") as! ScanDocumentVC
          present(myVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnScanCDomicilio(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ScanAddress", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ScanActaVC") as! ScanActaVC
                 present(myVC, animated: true, completion: nil)
    }
    
}
