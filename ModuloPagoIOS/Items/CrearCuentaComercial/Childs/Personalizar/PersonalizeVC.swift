//
//  PersonalizeVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 16/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class PersonalizeVC: UIViewController {

    @IBOutlet weak var viewPersonalizar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    private func setUpView(){
        viewPersonalizar.layer.cornerRadius = 10
        viewPersonalizar.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewPersonalizar.layer.shadowOpacity = 0.5
        viewPersonalizar.layer.shadowRadius = 4
        viewPersonalizar.clipsToBounds = false
        viewPersonalizar.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
    }
    
    
    @IBAction func onClickBack(_ sender: Any) {
      let storyboard = UIStoryboard(name: "Personalize", bundle: nil)
      let myVC = storyboard.instantiateViewController(withIdentifier: "LookQRVC") as! LookQRVC
      present(myVC, animated: true, completion: nil)
   
    }
    
    @IBAction func btnGuardar(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Personalize", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ProfileAssociateVC") as! ProfileAssociateVC
          present(myVC, animated: true, completion: nil)
    
    }


}
