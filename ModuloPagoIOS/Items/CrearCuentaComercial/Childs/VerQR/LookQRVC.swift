//
//  LookQRVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 16/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class LookQRVC: UIViewController {

    @IBOutlet weak var viewQR: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()

    }
    
    
       override var preferredStatusBarStyle: UIStatusBarStyle {
           return .lightContent
       }
    
    
    private func setUpView(){
           viewQR.layer.cornerRadius = 10
           viewQR.layer.shadowOffset = CGSize(width: 5, height: 5)
           viewQR.layer.shadowOpacity = 0.5
           viewQR.layer.shadowRadius = 4
           viewQR.clipsToBounds = false
           viewQR.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
       }
    
    
    @IBAction func onclickBack(_ sender: Any) {
        let storyboard = UIStoryboard(name: "LookQR", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ProfileAssociateVC") as! ProfileAssociateVC
          present(myVC, animated: true, completion: nil)
    }
    @IBAction func btnPersonalizar(_ sender: Any) {
        let storyboard = UIStoryboard(name: "LookQR", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PersonalizeVC") as! PersonalizeVC
          present(myVC, animated: true, completion: nil)
    }
    
}
