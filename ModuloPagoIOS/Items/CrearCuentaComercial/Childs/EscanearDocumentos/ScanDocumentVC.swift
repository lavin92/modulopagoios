//
//  ScanDocumentVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 15/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ScanDocumentVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func onClickBack(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ScanDoument", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "TradingAccountVC") as! TradingAccountVC
          present(myVC, animated: true, completion: nil)
    
    }
    @IBAction func btnScanIFE(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ScanDoument", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ScanAddressVC") as! ScanAddressVC
          present(myVC, animated: true, completion: nil)
    }
    
    
    

}
