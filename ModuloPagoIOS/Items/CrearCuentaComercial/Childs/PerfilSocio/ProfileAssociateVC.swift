//
//  ProfileAssociateVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 16/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ProfileAssociateVC: UIViewController {

    @IBOutlet weak var viewDetalleSucursal: UIView!
    @IBOutlet weak var viewSucursal: UIView!
    @IBOutlet weak var viewCaja: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    
    private func setUpView(){
        viewDetalleSucursal.layer.cornerRadius = 10
        viewDetalleSucursal.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewDetalleSucursal.layer.shadowOpacity = 0.5
        viewDetalleSucursal.layer.shadowRadius = 4
        viewDetalleSucursal.clipsToBounds = false
        viewDetalleSucursal.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        
        viewSucursal.layer.cornerRadius = 10
        viewSucursal.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewSucursal.layer.shadowOpacity = 0.5
        viewSucursal.layer.shadowRadius = 4
        viewSucursal.clipsToBounds = false
        viewSucursal.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
        
        viewCaja.layer.cornerRadius = 10
        viewCaja.layer.shadowOffset = CGSize(width: 5, height: 5)
        viewCaja.layer.shadowOpacity = 0.5
        viewCaja.layer.shadowRadius = 4
        viewCaja.clipsToBounds = false
        viewCaja.layer.shadowColor = UIColor(red: 47.0/255.0, green: 79.0/255.0, blue: 79.0/255.0, alpha: 1.0).cgColor
    }
    
    
    @IBAction func onclickBack(_ sender: Any) {
           let storyboard = UIStoryboard(name: "ProfileAssociate", bundle: nil)
           let myVC = storyboard.instantiateViewController(withIdentifier: "AssociateAccountVC") as! AssociateAccountVC
             present(myVC, animated: true, completion: nil)
       }
    @IBAction func btnVerQR(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ProfileAssociate", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "LookQRVC") as! LookQRVC
          present(myVC, animated: true, completion: nil)
        
    }
    
    @IBAction func irMenu(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ProfileAssociate", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "PaymentModuleVC") as! PaymentModuleVC
          present(myVC, animated: true, completion: nil)
        
    }
    
}
