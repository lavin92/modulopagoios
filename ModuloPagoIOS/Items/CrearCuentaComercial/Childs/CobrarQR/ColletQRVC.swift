//
//  ColletQRVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 16/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class ColletQRVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private func setUpView(){
       
    }
    
    
    @IBAction func onclickBack(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ColletQR", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "AssociateAccountVC") as! AssociateAccountVC
          present(myVC, animated: true, completion: nil)
    }

}
