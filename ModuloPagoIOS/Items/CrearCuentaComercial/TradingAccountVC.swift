//
//  TradingAccountVC.swift
//  ModuloPagoIOS
//
//  Created by antonio lavin on 14/07/20.
//  Copyright © 2020 TotalPlay. All rights reserved.
//

import UIKit

class TradingAccountVC: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func clickBack(_ sender: Any) {
        let storyboard = UIStoryboard(name: "TradingAccount", bundle: nil)
               let myVC = storyboard.instantiateViewController(withIdentifier: "LoginTPVC") as! LoginTPVC
                 present(myVC, animated: true, completion: nil)
    }
    @IBAction func clickConfirmarDatos(_ sender: Any) {
        let storyboard = UIStoryboard(name: "TradingAccount", bundle: nil)
        let myVC = storyboard.instantiateViewController(withIdentifier: "ScanDocumentVC") as! ScanDocumentVC
          present(myVC, animated: true, completion: nil)
    }
    
}
